# Candy sorting machine

Sorting sweets (like M&Ms) by color or shape.

## Goal

Machine has 3 operating modes: sorting by color, sorting by shape and manual mode.

## Software

Raspberry Pi: Python 3.6 + OpenCV (color mode) and Keras with TensorFlow backend (shape mode)

Husarion Core: C with Husarion libraries

## Hardware

Raspberry Pi 3B+ with Raspberry Pi Camera HD v2 8MPx (detecting parts and sending signal to Husarion)

Husarion Core 2.0 (operating of LEGO motors and sensors)

## Mechanics

Machine build with LEGO and 3D printed parts.

Consists of pusher, 2 conveyor belts and rotary table.

## Prerequisites
```
sudo apt-get install -y libhdf5-dev libhdf5-serial-dev libatlas-base-dev libjasper-dev libqtgui4 libqt4-test
sudo pip3 install opencv-python
wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v1.8.0/tensorflow-1.8.0-cp35-none-linux_armv7l.whl
sudo pip3 install tensorflow-1.8.0-cp35-none-linux_armv7l.whl
sudo pip3 uninstall mock
sudo pip3 install mock
```
## Running
```
python3 GUI.py
```

- preparing data

from saved images
```
python3 parse_images.py
```
or generating images on Raspberry
```
python3 image_generator.py
python3 change_filenames.py
python3 csv_generator
```
- training network model
```
python3 network_training.py
```
- running TensorBoard
```
tensorboard --logdir=log
```