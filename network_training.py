import keras
from keras import layers
from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

""" Training the neural network model """

# 2000 training images and 200 validation images

# params
epochs = 100
steps_per_epoch = 100
validation_steps = 10
size = (128, 128, 1)

# paths
path_base = r'/content/drive/My Drive/lego/'
path_csv = path_base + 'csv/data.csv'
path_csv_val = path_base + 'csv/data_val.csv'
path_image = path_base + 'parsed/'
path_image_val = path_base + 'parsed_val/'
path_model = path_base + 'model/'

# helper functions from keras and tensorflow
callbacks = [
    keras.callbacks.TensorBoard(           # TensorBoard - the visualisation
        log_dir='logs',
        histogram_freq=1,
        embeddings_freq=1,
        embeddings_data=np.arange(0, size[0] * size[1]).reshape((1, size[0] * size[1])),
    ),

    keras.callbacks.EarlyStopping(          # stopping the learning process after pre-training
        monitor='acc',
        patience=5,
    ),

    keras.callbacks.ModelCheckpoint(        # saving the best model only
        filepath='model.h5',
        monitor='val_loss',
        save_best_only=True,
    ),

    keras.callbacks.ReduceLROnPlateau(      # escaping the local minimum by improving learning rate
        monitor='val_loss',
        factor=0.1,
        patience=5,
    ),
]

model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=size))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Flatten())
model.add(layers.Dropout(0.2))
model.add(layers.Dense(6, activation='sigmoid'))

model.summary()

optimizer = optimizers.Adam()
model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

# reading data
df = pd.read_csv(path_csv, sep=',', header=0, names=['filename', 'class'])
df_val = pd.read_csv(path_csv_val, sep=',', header=0, names=['filename', 'class'])

# preparation of data, augmentation
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    rotation_range=360,
    width_shift_range=0.3,
    height_shift_range=0.3,
    shear_range=0.3,
    zoom_range=0.3,
    horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1. / 255)

# generator of training data
train_generator = train_datagen.flow_from_dataframe(
    dataframe=df,
    directory=path_image,
    x_col='filename',
    y_col='class',
    target_size=(size[0], size[1]),
    shuffle=True,
    class_mode='categorical',
    color_mode='grayscale')

# generator of val data
validation_generator = test_datagen.flow_from_dataframe(
    dataframe=df_val,
    directory=path_image_val,
    x_col='filename',
    y_col='class',
    target_size=(size[0], size[1]),
    shuffle=True,
    class_mode='categorical',
    color_mode='grayscale')

# training
history = model.fit_generator(
    train_generator,
    steps_per_epoch=steps_per_epoch,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=validation_steps,
    callbacks=callbacks)

# plot
acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'bo', label='Train accuracy')
plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
plt.title('The accuracy of training and validation')
plt.legend()

plt.show()

# saving the model
model_json = model.to_json()
with open(path_model + 'model.json', 'w') as json_file:
    json_file.write(model_json)

print('Saved model to disk')
