import serial

""" Class for stopping and starting the device """


class Stop:

    def __init__(self):
        pass

    def stop(self):
        port = serial.Serial(port="/dev/ttyAMA0", baudrate=11520,
                             parity=serial.PARITY_NONE,
                             stopbits=serial.STOPBITS_ONE,
                             timeout=1)

        port.write(b'P')
        port.close()
        print('STOP')

    def start(self):
        port = serial.Serial(port="/dev/ttyAMA0", baudrate=11520,
                             parity=serial.PARITY_NONE,
                             stopbits=serial.STOPBITS_ONE,
                             timeout=1)

        port.write(b'T')
        port.close()
        print('START')