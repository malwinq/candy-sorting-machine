import object_tracking as color
import shape_prediction as shape


if __name__ == '__main__':

    colors = color.Color()
    shapes = shape.Shape()
    mode = int(input())

    if mode == 1:
        colors.detect()
    elif mode == 0:
        shapes.detect()

