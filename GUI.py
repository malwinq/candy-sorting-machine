from tkinter import *
import object_tracking as color
import shape_prediction as shape
import stop

""" GUI to control the sorting process"""

shapes = shape.Shape()
colors = color.Color()
i = 0


def shape_command():
    global shapes
    shapes.detect()


def color_command():
    global colors
    colors.detect()


def start_stop_command():
    st = stop.Stop()
    global i
    if i == 0:
        st.stop()
        i = 1
    elif i == 1:
        st.start()
        i = 0


if __name__ == '__main__':

    font = ('Arial Bold', 20)

    window = Tk()
    window.title("CANDY SORTING MACHINE")
    window.geometry('700x300')

    # title
    lbl1 = Label(window, text='Choose the sorting type',
                font=('Arial Bold', 30), fg='blue')
    lbl1.place(x=100, y=30)

    lbl2 = Label(window, text='To escape the type, press Esc',
                font=('Arial Bold', 20), fg='blue')
    lbl2.place(x=95, y=90)

    # buttons
    btn_shape = Button(window, text='Shape',
                       bg='blue4', fg='white',
                       height=2, width=10,
                       font=font, command=shape_command)
    btn_shape.place(x=407, y=160)
    btn_color = Button(window, text='Color',
                       bg='blue4', fg='white',
                       height=2, width=10,
                       font=font, command=color_command)
    btn_color.place(x=100, y=160)
    # btn_stop = Button(window, text='Start/Stop',
    #                   bg='red', fg='black',
    #                   height=2, width=10,
    #                   font=font, command=start_stop_command)
    # btn_stop.place(x=250, y=280)

    window.mainloop()
