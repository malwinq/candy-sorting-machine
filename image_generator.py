import cv2
import time

""" Script for making photos by PiCamera """

# 1 mms, 2 chewing gum, 3 haribo bears
# 4 krakersy, 5 biscuits, 6 cheerios

# params
blur_size = 9                                               # the parameter of filter
size = 128                                                  # photo size
path_base = r'/home/pi/candy-sorting-machine/'
# path_base = r'/home/malwina/repo/candy-sorting-machine/'
path_images = path_base + r'images_v2/'

# reading stream from camera
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, size)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, size)
i = 1

while cap.isOpened():

    _, frame = cap.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)         # conversion to grayscale
    frame = cv2.medianBlur(frame, 9)                        # blurring

    cv2.imshow('camera', frame)

    path = path_images + str(i) + '.jpg'                    # path to folder
    cv2.imwrite(path, frame)                                # saving image

    i += 1
    print(i)

    # waiting for esc
    wait_key = cv2.waitKey(1)
    if wait_key == 27:
        break

    time.sleep(0.2)

cap.release()
cv2.destroyAllWindows()
