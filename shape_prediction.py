from keras.models import model_from_json
from keras.preprocessing import image
import cv2
import numpy as np
import serial
import time

""" Prediction of shape based on pre-trained neural network model (acc 91%) """


class Shape:

    def __init__(self):
        path_base = r'/home/pi/candy-sorting-machine/'
        # path_base = r'/home/malwina/repo/candy-sorting-machine/'
        path_model = path_base + 'model/'

        self.size = 128
        self.min_pred = 0.008

        self.class_name = ['biscuit', 'cheerios', 'gum', 'haribo', 'precel', 'mms']
        self.class_signal = ['A', 'B', 'C', 'D', 'E', 'F']

        json_file = open(path_model + 'model.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()

        self.model = model_from_json(loaded_model_json)
        self.model.load_weights(path_model + 'model.h5')

        print('Loaded model from disk')


    def detect(self):

        print('SHAPE')

        cap = cv2.VideoCapture(0)

        port = serial.Serial(port="/dev/ttyAMA0", baudrate=115200,
                                 parity=serial.PARITY_NONE,
                                 stopbits=serial.STOPBITS_ONE,
                                 timeout=1)

        signal = str('S').encode('ascii')
        port.write(signal)
        print('Start')
        time.sleep(0.05)

        while cap.isOpened():

            ret, org_frame = cap.read()
            frame = cv2.resize(org_frame, (self.size, self.size))
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = cv2.medianBlur(frame, 9)
            frame = image.img_to_array(frame)
            frame = frame / 255
            frame = np.expand_dims(frame, axis=0)

            predicts = self.model.predict(frame)
            prop = np.amax(predicts)
            ind = np.argmax(predicts)

            if prop > self.min_pred:

                sweets_class = self.model.predict_classes(frame)
                text = self.class_name[int(sweets_class[0])]
                org_frame = cv2.putText(org_frame, text, (2, 20), cv2.FONT_HERSHEY_COMPLEX, 0.7, (0, 0, 0), 2)

                return_signal = self.class_signal[int(ind)]

            else:
                return_signal = 'Z'

            print(return_signal)

            signal = str(return_signal).encode('ascii')
            port.write(signal)

            cv2.imshow('camera', org_frame)

            wait_key = cv2.waitKey(1)
            if wait_key == 27:
                break

            time.sleep(0.05)

        signal = str('X').encode('ascii')
        port.write(signal)
        time.sleep(0.05)

        cap.release()
        port.close()
        cv2.destroyAllWindows()

        return None
