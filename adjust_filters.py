import cv2
import numpy as np

""" Script for adjusting parameters of masking and filtering data for neural network """


# callback function
def nothing(x):
    pass


img = cv2.imread('sweets.jpg', cv2.IMREAD_GRAYSCALE)
img = cv2.resize(img, (256, 256))
# hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

cv2.namedWindow('thresholding')
cv2.createTrackbar('thres_min', 'thresholding', 0, 255, nothing)
cv2.createTrackbar('kernel1', 'thresholding', 0, 10, nothing)
cv2.createTrackbar('kernel2', 'thresholding', 0, 10, nothing)
cv2.createTrackbar('iterations', 'thresholding', 0, 10, nothing)
cv2.createTrackbar('size', 'thresholding', 1, 80, nothing)

while True:
    thres_min = cv2.getTrackbarPos('thres_min', 'thresholding')
    ker1 = cv2.getTrackbarPos('kernel1', 'thresholding')
    ker2 = cv2.getTrackbarPos('kernel2', 'thresholding')
    iterations = cv2.getTrackbarPos('iterations', 'thresholding')
    tmp_size = cv2.getTrackbarPos('size', 'thresholding')
    size = tmp_size if tmp_size % 2 == 1 else tmp_size + 1

    kernel1 = np.ones((ker1, ker1), np.uint8)
    kernel2 = np.ones((ker2, ker2), np.uint8)

    _, mask = cv2.threshold(img, thres_min, 255, cv2.THRESH_BINARY)
    mask = cv2.erode(mask, kernel1, iterations=iterations)
    mask = cv2.dilate(mask, kernel2, iterations=iterations)
    median = cv2.medianBlur(mask, size)

    titles = ['image', 'mask', 'median']
    images = [img, mask, median]

    for i in range(len(images)):
        cv2.imshow(titles[i], images[i])

    key = cv2.waitKey(1)
    if key == 27:
        break

cv2.destroyAllWindows()
