import os

""" Changing filenames of photos in target folder """

class_name = ['grzeski', 'haribo', 'mms', 'picoballa', 'precle', 'none']
path_base = r'/home/malwina/repo/candy-sorting-machine/'
path_images = path_base + r'images_v2/'

max_ind = 3000

for i in class_name:
    path_folder = path_images + i + '/'
    counter = 1
    print(i)

    for j in range(max_ind):
        tmp_path = path_folder + str(j) + '.jpg'
        if i == 'picoballa':
            tmp_path = path_folder + 'picoballa' + str(j + 1) + '.jpg'
        to_path = path_folder + str(counter) + '.jpg'

        try:
            os.rename(tmp_path, to_path)
            print(counter)
            counter += 1
        except FileNotFoundError:
            continue

print('Done')
