import cv2
import csv

""" Script for parsing photos made by smartphone:
    1. Changing size to PiCamera's size
    2. Sharpening edges
    3. Preparing csv file for training the neural network model """

# 1 mms, 2 chewing gum, 3 haribo bears
# 4 krakersy, 5 biscuits, 6 cheerios

# params
classes = 6                 # liczba klas
blur_size = 9               # parametr filtru
img_size = (128, 128)       # rozmiar zdjecia
val_percent = 18            # % danych do walidacji
path_base = r'/home/malwina/repo/candy-sorting-machine/'

# the number of photos in each folder
len_list = [0] * classes
len_list[0] = 310                 # mms
len_list[1] = 256                 # chewing gum
len_list[2] = 456                 # haribo
len_list[3] = 273                 # krakersy
len_list[4] = 361                 # biscuits
len_list[5] = 143                 # cheerios
# all = 1798

# paths to save
path_to_save = path_base + 'parsed/'
path_to_save_val = path_base + 'parsed_val/'
path_csv = path_base + 'csv/data.csv'
path_csv_val = path_base + 'csv/data_val.csv'

columns = ['filename', 'class']
class_name = ['mms', 'gum', 'haribo', 'krakers', 'biscuits', 'cheerios']
counter = 0
data = []
data_val = []

for i in range(classes):
    path_folder = path_base + 'images/' + str(i + 1) + '/'

    for j in range(len_list[i]):
        row = []

        path_image = path_folder + str(j + 1) + '.jpg'
        img = cv2.imread(path_image, cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img, img_size)
        img = cv2.medianBlur(img, blur_size)

        if (counter + 1) % val_percent != 0:
            tmp_path = path_to_save + str(counter) + '.jpg'
            cv2.imwrite(tmp_path, img)

            row.append(str(counter) + '.jpg')
            row.append(class_name[i])

            data.append(row)

        else:
            tmp_path = path_to_save_val + str(counter) + '.jpg'
            cv2.imwrite(tmp_path, img)

            row.append(str(counter) + '.jpg')
            row.append(class_name[i])

            data_val.append(row)

        print(counter)
        counter += 1

with open(path_csv, mode='w') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=',')
    csv_writer.writerow(columns)

    for i in range(len(data)):
        csv_writer.writerow(data[i])

with open(path_csv_val, mode='w') as csv_file_val:
    csv_writer_val = csv.writer(csv_file_val, delimiter=',')
    csv_writer_val.writerow(columns)

    for i in range(len(data_val)):
        csv_writer_val.writerow(data_val[i])

print('Done')
