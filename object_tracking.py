import cv2
import numpy as np
from math import sqrt
import serial
import time

""" Detecting colors from video capture """


class Color:

    def __init__(self):

        # params
        self.color = (255, 255, 255)                                     # text color
        self.rect = np.ones([3, 3], np.uint8) * 255                      # rectangle for first masking
        self.rect2 = np.ones([10, 10], np.uint8) * 255                   # rectangle for second masking

        # color names
        self.colors = ['green', 'blue', 'yellow', 'orange', 'brown', 'red', 'not_white']

        # dictionary of signals
        self.signals = {'green': 'A', 'blue': 'B', 'yellow': 'C', 'orange': 'D', 'brown': 'E', 'red': 'F'}

        # borders of colors

        # NOT-WHITE
        wh_l = [0, 9, 0]
        wh_u = [138, 255, 255]

        # ORANGE
        or_l = [0, 114, 81]
        or_u = [11, 158, 255]

        # YELLOW
        ye_l = [11, 74, 82]
        ye_u = [26, 174, 243]

        # GREEN
        gr_l = [39, 38, 59]
        gr_u = [82, 190, 153]

        # BLUE
        bl_l = [99, 57, 54]
        bl_u = [122, 202, 191]

        # RED
        re_l = [175, 120, 102]
        re_u = [184, 178, 162]

        # BROWN
        br_l = [167, 81, 53]
        br_u = [180, 121, 86]

        # ---------------------------------------------------------------------
        self.lower = {}
        self.upper = {}

        self.lower['not_white'] = np.array([wh_l[0], wh_l[1], wh_l[2]])
        self.upper['not_white'] = np.array([wh_u[0], wh_u[1], wh_u[2]])

        self.lower['orange'] = np.array([or_l[0], or_l[1], or_l[2]])
        self.upper['orange'] = np.array([or_u[0], or_u[1], or_u[2]])

        self.lower['yellow'] = np.array([ye_l[0], ye_l[1], ye_l[2]])
        self.upper['yellow'] = np.array([ye_u[0], ye_u[1], ye_u[2]])

        self.lower['green'] = np.array([gr_l[0], gr_l[1], gr_l[2]])
        self.upper['green'] = np.array([gr_u[0], gr_u[1], gr_u[2]])

        self.lower['blue'] = np.array([bl_l[0], bl_l[1], bl_l[2]])
        self.upper['blue'] = np.array([bl_u[0], bl_u[1], bl_u[2]])

        self.lower['red'] = np.array([re_l[0], re_l[1], re_l[2]])
        self.upper['red'] = np.array([re_u[0], re_u[1], re_u[2]])

        self.lower['brown'] = np.array([br_l[0], br_l[1], br_l[2]])
        self.upper['brown'] = np.array([br_u[0], br_u[1], br_u[2]])

    def detect(self):

        print('COLOR')

        cap = cv2.VideoCapture(0)

        port = serial.Serial(port="/dev/ttyAMA0", baudrate=115200,
                            parity=serial.PARITY_NONE,
                            stopbits=serial.STOPBITS_ONE,
                            timeout=1)

        signal = str('S').encode('ascii')                           # START signal
        port.write(signal)
        print('Start')
        time.sleep(0.05)

        return_signal = 'Z'                                         # Z by default means 'not-classified'

        mask = {}
        moments = {}
        x_m = {}
        y_m = {}
        text = {}
        size = {}
        p1 = {}
        p2 = {}

        is_detected = {}

        while cap.isOpened():

            req, frame = cap.read()

            hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

            # MASKING
            for key in self.colors:

                mask[key] = cv2.inRange(hsv, self.lower[key], self.upper[key])

                # filtering
                mask[key] = cv2.erode(mask[key], self.rect, iterations=2)
                mask[key] = cv2.dilate(mask[key], self.rect2, iterations=2)

                # calculating the center point of objects
                moments[key] = cv2.moments(mask[key])
                text[key] = key.upper()

                # if the color is not detected of the detection is too small
                if moments[key]['m00'] < 1000000.0:
                    is_detected[key] = 0

                else:
                    is_detected[key] = 1

                    # position of object
                    x_m[key] = moments[key]['m10'] / moments[key]['m00']
                    y_m[key] = moments[key]['m01'] / moments[key]['m00']

                    text[key] = text[key] + ' x: ' + str(int(x_m[key])) + ', y: ' + str(int(y_m[key]))

                    size[key] = int(sqrt(moments[key]['m00']) / 40)

                    p1[key] = (int(x_m[key]) - size[key], int(y_m[key]) - size[key])
                    p2[key] = (int(x_m[key]) + size[key], int(y_m[key]) + size[key])

                    # drawing the rectangle around detected object
                    frame = cv2.rectangle(frame, p1[key], p2[key], self.color, 2)

                    frame = cv2.putText(frame, text[key], p1[key], cv2.FONT_HERSHEY_COMPLEX, 1, self.color, 2)

            # sending the return signal
            sum_k = 0
            for key, item in is_detected.items():               # checking how many colors were detected
                if key != 'not_white':
                    sum_k += item

            if sum_k == 1:
                for key, item in is_detected.items():
                    if item == 1 and key != 'not_white':
                        return_signal = self.signals[key]
            elif is_detected['not_white'] == 1:
                return_signal = 'Z'

            print(return_signal)

            # SENDING SIGNAL
            signal = str(return_signal).encode('ascii')          # encoding to ascii
            port.write(signal)

            cv2.imshow('camera', frame)

            wait_key = cv2.waitKey(1)
            if wait_key == 27:
                break

            time.sleep(0.05)

        signal = str('X').encode('ascii')                         # STOP signal
        port.write(signal)
        time.sleep(0.05)

        cap.release()
        port.close()
        cv2.destroyAllWindows()

        return None
