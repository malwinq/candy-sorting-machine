import keras
from keras.models import model_from_json
from keras.preprocessing import image
import cv2
import numpy as np

""" Predicton of shape based on pre-trained neural network model (acc 91%) """

path_base = r'/home/malwina/repo/candy-sorting-machine/'
path_model = path_base + 'model/'
path_images = path_base + 'images/'
size = 128
min_pred = 0.7          # limit of prediction

# PROPER CLASS NAMES
class_name = ['biscuits', 'cheerios', 'gum', 'haribo', 'krakers', 'mms']

# reading the model files
json_file = open(path_model + 'model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()

model = model_from_json(loaded_model_json)
model.load_weights(path_model + 'model.h5')

print('Loaded model from disk')

org_frame = cv2.imread(path_images + '1/150.jpg', cv2.IMREAD_GRAYSCALE)
org_frame = cv2.resize(org_frame, (size, size))
frame = cv2.medianBlur(org_frame, 9)

frame = frame / 255

frame = image.img_to_array(frame)
frame = np.expand_dims(frame, axis=0)

sweets_class = model.predict_classes(frame)
# print('class: ' + str(sweets_class))
print('class: ' + class_name[int(sweets_class[0])])

text = class_name[int(sweets_class[0])]
org_frame = cv2.putText(org_frame, text, (10, 10), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255), 2)

cv2.destroyAllWindows()
