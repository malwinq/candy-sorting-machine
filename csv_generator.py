import csv
import cv2

""" Script for generating csv file based on PiCamera images """

# 1 mms, 2 chewing gum, 3 haribo bears
# 4 krakersy, 5 biscuits, 6 cheerios

# params
path_base = r'/home/malwina/repo/candy-sorting-machine/'
path_images = path_base + r'images_v2/'
path_csv = path_base + r'csv_v2/data.csv'
path_csv_val = path_base + r'csv_v2/data_val.csv'
path_to_save = path_images + r'all/'
path_to_save_val = path_images + r'all_val/'
classes = 5                                           # number of classes
val_percent = 7                                       # 1/val_percent - the % of data which will be the validation set

# the number of photos in each folder
len_list = {}
len_list['grzeski'] = 660
len_list['haribo'] = 600
len_list['mms'] = 995
len_list['picoballa'] = 901
len_list['precle'] = 935
len_list['none'] = 862

columns = ['filename', 'class']                                 # columns of csv file
class_name = ['grzeski', 'haribo', 'mms', 'picoballa', 'precle', 'none']
counter = 0
data = []
data_val = []

# loop of classes
for i in class_name:
    path_folder = path_images + str(i) + '/'                     # changing the path to folder

    # loop of photos
    for j in range(len_list[i]):
        row = []                                                 # row of csv file

        path_photo = path_folder + str(j + 1) + '.jpg'           # path to every photo

        # saving the data
        # training set
        if (counter + 1) % val_percent != 0:

            # saving all photos in one directory
            tmp_path = path_to_save + str(counter) + '.jpg'       # changing path
            cv2.imwrite(tmp_path, cv2.imread(path_photo))         # save to file

            # row saving
            row.append(str(counter) + '.jpg')
            row.append(i)

            # saving row to array of data to csv file
            data.append(row)

        # validation data
        else:

            tmp_path = path_to_save_val + str(counter) + '.jpg'
            cv2.imwrite(tmp_path, cv2.imread(path_photo))

            row.append(str(counter) + '.jpg')
            row.append(i)

            data_val.append(row)

        print(counter)
        counter += 1

# writing to csv file
# training set
with open(path_csv, mode='w') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=',')
    csv_writer.writerow(columns)

    for i in range(len(data)):
        csv_writer.writerow(data[i])

# val set
with open(path_csv_val, mode='w') as csv_file_val:
    csv_writer_val = csv.writer(csv_file_val, delimiter=',')
    csv_writer_val.writerow(columns)

    for i in range(len(data_val)):
        csv_writer_val.writerow(data_val[i])

print('Done')
